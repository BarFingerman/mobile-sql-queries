
set @PROJECT =  'ccru';
set @FROM = '2017-06-19 00:00:00'; #CURDATE();
set @TO = '2017-06-20 00:00:00';


SELECT 
    session_created.project_name AS PROJECT,
    TIME_TO_SEC(TIMEDIFF(last_uploaded.LAST_UPLOADED_TIME, session_created.timestamp)) AS UPLOAD_DURATION,
    session_created.timestamp as SESSION_CREATED,
	last_uploaded.LAST_UPLOADED_TIME,
    session_created.session_uid AS uid
FROM
    (SELECT 
        project_name,
            session_uid,
            timestamp 
    FROM
        eventsdb.events
    WHERE
        timestamp >= @FROM 
        AND timestamp < @TO
            AND event_name = 'SESSION_CREATED'
            AND project_name = @PROJECT) AS session_created,
    (SELECT 
            session_uid,
            MAX(timestamp) as LAST_UPLOADED_TIME
    FROM
        eventsdb.events
    WHERE
        timestamp >= @FROM 
		AND event_name = 'PROBE_UPLOADED'
		AND project_name = @PROJECT
	GROUP BY session_uid) AS last_uploaded
WHERE
        last_uploaded.session_uid = session_created.session_uid
