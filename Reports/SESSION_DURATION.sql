
set @PROJECT =  'ccru';
set @FROM = '2017-06-19 00:00:00'; #CURDATE();
set @TO = '2017-06-20 00:00:00';

SELECT 
    session_started.project_name AS PROJECT,
    TIME_TO_SEC(TIMEDIFF(session_created.timestamp, session_started.timestamp)) AS SESSSION_DURATION,
    session_started.timestamp as SESSION_STARTED,
    session_created.timestamp as SESSION_CREATED,
    session_started.session_uid AS uid
FROM
    (SELECT 
        project_name,
            session_uid,
            timestamp 
    FROM
        eventsdb.events
    WHERE
        timestamp >= @FROM 
        AND timestamp < @TO
            AND event_name = 'SESSION_STARTED'  #'SESSION_AGENT-CREATED'
            AND project_name = @PROJECT) AS session_started,
    (SELECT 
        project_name,
            session_uid,
            timestamp 
    FROM
        eventsdb.events
    WHERE
        timestamp >= @FROM 
		AND event_name = 'SESSION_PENDING-UPLOAD'  #'SESSION_AGENT-PENDING-UPLOAD'
		AND project_name = @PROJECT) AS session_created
WHERE
		
        session_started.session_uid = session_created.session_uid
        AND session_started.timestamp IS NOT NULL
        AND session_created.timestamp IS NOT NULL