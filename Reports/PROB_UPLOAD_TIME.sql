
set @PROJECT =  'ccru';
set @FROM = '2017-06-19 00:00:00'; #CURDATE();
set @TO = '2017-06-20 00:00:00';

SELECT 
    Probe_Captured.project_name AS PROJECT,
    TIME_TO_SEC(TIMEDIFF( Probe_Uploaded.PROBE_UPLOADED_TIME ,Probe_Captured.PROBE_IMAGE_CAPTURED_TIME
                           )) AS TOTAL_UPLOAD_TIME_IN_SECONDS,
    Probe_Captured.PROBE_IMAGE_CAPTURED_TIME,
    Probe_Uploaded.PROBE_UPLOADED_TIME,
    Probe_Captured.image_uid AS uid
FROM
    (SELECT 
        project_name,
            image_uid,
            session_uid,
            timestamp AS PROBE_IMAGE_CAPTURED_TIME
    FROM
        eventsdb.events
    WHERE
        timestamp >= @FROM 
        AND timestamp < @TO
		AND event_name = 'PROBE-IMAGE_CAPTURED' #'PROBE-IMAGE_AGENT-CAPTURED' 
		AND project_name = @PROJECT) AS Probe_Captured,
    (SELECT 
        project_name,
            image_uid,
            session_uid,
            timestamp AS PROBE_UPLOADED_TIME
    FROM
        eventsdb.events
    WHERE
        timestamp >= @FROM 
		AND event_name = 'PROBE-IMAGE_SENT'  #'PROBE-IMAGE_AGENT-SAVED'
		AND project_name = @PROJECT) AS Probe_Uploaded
WHERE
    Probe_Captured.image_uid = Probe_Uploaded.image_uid
        AND Probe_Captured.session_uid = Probe_Uploaded.session_uid
        AND Probe_Captured.PROBE_IMAGE_CAPTURED_TIME IS NOT NULL
        AND Probe_Uploaded.PROBE_UPLOADED_TIME IS NOT NULL