select temp.project_name as project, temp.period 
						 as time_dif 
                         
from ( SELECT project_name, 
			  json_extract(json_event, '$.upload_duration') as period 

       FROM eventsdb.events 
       
       where date(timestamp) >= date(sysdate()) - interval 1 DAY and date(timestamp) < date(sysdate()) 
			 and event_name = 'PROBE-IMAGE-RECEIVED' 
			 and project_name in ('hbcde')) 
			 as temp 
            
where temp.period is not NULL;