select temp.project_name as project, 
	   TIME_TO_SEC(timediff(max(temp.timestamp),
       min(temp.timestamp))) as time_dif 

from ( SELECT project_name, 
			  Scene_uid, 
              timestamp 

	   FROM eventsdb.events 
       
       where date(timestamp) >= date(sysdate()) - interval 1 DAY and date(timestamp) < date(sysdate()) 
		 	 and event_name = 'PROBE-IMAGE-RECEIVED' 
             and cast(json_extract(json_event, '$.upload_duration') as SIGNED) is not null 
		 	 and project_name in ('abica', 'abinbevuk', 'abitamsus-prod', 'ahold', 'americas-demo', 'batru', 'bikr', 'bimy', 'biph', 'biseask', 'bith', 'bitr', 'bivn', 'borjomiru', 'campina-prod', 'carrefourar', 'ccaau', 'ccabiza', 'ccanz', 'ccbr-prod', 'ccede', 'ccil', 'ccjp', 'cckr', 'cclc', 'ccmy', 'ccph', 'ccrpr-prod', 'ccru', 'ccrus', 'ccth', 'cctradmx', 'ccus', 'ccus-demo', 'ccvmjp', 'ccza', 'diageoau', 'diageobr', 'diageouk', 'emea-demo', 'env34', 'ferrerokr', 'hbcde', 'heinekencn', 'heinekengr', 'heinekenus-prod', 'heinzau', 'inbevar', 'inbevbe', 'InbevBr', 'InbevBr_Ontrade', 'InBevBR_Posters', 'inbevcl', 'inbevcn', 'inbevcoolerar', 'inbevcoolerus', 'inbevkr', 'inbevmx', 'inbevpy', 'inbevus', 'kcmy', 'kcus', 'labattplnoptca', 'lavazzaat', 'lavazzaau', 'lavazzabe', 'lavazzabg', 'lavazzaca', 'lavazzach', 'lavazzacl', 'lavazzacn', 'lavazzacrfr', 'lavazzacz', 'lavazzade', 'lavazzadk', 'lavazzaee', 'lavazzaes', 'lavazzafr', 'lavazzagr', 'lavazzahk', 'lavazzahr', 'lavazzail', 'lavazzait', 'lavazzajp', 'lavazzakr', 'lavazzalt', 'lavazzalu', 'lavazzalv', 'lavazzama', 'lavazzanl', 'lavazzapl', 'lavazzaro', 'lavazzaru', 'lavazzasa', 'lavazzase', 'lavazzasg', 'lavazzauae', 'lavazzauk', 'lavazzaus', 'lavazzaza', 'lionau-prod', 'lrsuk', 'marsbr', 'marsfr-prod', 'marsin-prod', 'marsru-prod', 'mayoraid', 'molsoncoolerca', 'molsoncoolerhr', 'molsoncoolermx', 'molsoncoorsuk', 'mondelezfr', 'ncn', 'nestlefr', 'nestleuk', 'niveauk', 'nrf2017-demo', 'one-americas-demo', 'one-apac-demo', 'pepsicoau', 'pepsicoolerus-prod', 'pmiua', 'pngau', 'pngcn-prod', 'pngjp', 'pngmccn', 'pngoccn-prod', 'pngza', 'rbde', 'rbus', 'rcuk', 'reachuk', 'repngcn-demo', 'ri-diageoau', 'rialcbevuk-prod', 'ripetcareuk-prod', 'risparkwinede', 'rius', 'rnbde', 'rnbse', 'shelluk', 'smartcooler2-trn', 'solarbr', 'static-camera-demo', 'swirecn', 'tnuvail', 'traxdemo-ts-prod')) 
			 as temp 
             
	   group by temp.project_name, 
				temp.Scene_uid 
       
       order by time_dif desc;