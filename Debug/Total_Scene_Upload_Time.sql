#-----------------------------------------------------------------------
select SCENE_STARTED.project_name as PROJECT, 
       SCENE_STARTED.SCENE_CAPTURED_TIME, 
       SCENE_CREATED.SCENE_CREATED_TIME, 
       TIME_TO_SEC(TIMEDIFF(SCENE_CREATED.SCENE_CREATED_TIME, 
                                SCENE_STARTED.SCENE_CAPTURED_TIME)) as TOTAL_UPLOAD_TIME_IN_SECONDS
 
from (
 
    #-----------------------------------------------------------------------
	SELECT project_name,
           scene_uid, 
           session_uid,
           CONVERT_TZ(substr((json_extract(json_event, '$.timestamp')), 2, 19), 
                      substr(json_extract(json_event, '$.timestamp'),21, 6),'+00:00')  as SCENE_CAPTURED_TIME

	FROM eventsdb.events
		
	WHERE date(timestamp) = date(sysdate())
	      and event_name = 'SCENE_STARTED'
	      and project_name in ('abica', 'abinbevuk', 'abitamsus-prod', 'ahold', 'americas-demo', 'batru', 'bikr', 'bimy', 'biph', 'biseask', 'bith', 'bitr', 'bivn', 'borjomiru', 'campina-prod', 'carrefourar', 'ccaau', 'ccabiza', 'ccanz', 'ccbr-prod', 'ccede', 'ccil', 'ccjp', 'cckr', 'cclc', 'ccmy', 'ccph', 'ccrpr-prod', 'ccru', 'ccrus', 'ccth', 'cctradmx', 'ccus', 'ccus-demo', 'ccvmjp', 'ccza', 'diageoau', 'diageobr', 'diageouk', 'emea-demo', 'env34', 'ferrerokr', 'hbcde', 'heinekencn', 'heinekengr', 'heinekenus-prod', 'heinzau', 'inbevar', 'inbevbe', 'InbevBr', 'InbevBr_Ontrade', 'InBevBR_Posters', 'inbevcl', 'inbevcn', 'inbevcoolerar', 'inbevcoolerus', 'inbevkr', 'inbevmx', 'inbevpy', 'inbevus', 'kcmy', 'kcus', 'labattplnoptca', 'lavazzaat', 'lavazzaau', 'lavazzabe', 'lavazzabg', 'lavazzaca', 'lavazzach', 'lavazzacl', 'lavazzacn', 'lavazzacrfr', 'lavazzacz', 'lavazzade', 'lavazzadk', 'lavazzaee', 'lavazzaes', 'lavazzafr', 'lavazzagr', 'lavazzahk', 'lavazzahr', 'lavazzail', 'lavazzait', 'lavazzajp', 'lavazzakr', 'lavazzalt', 'lavazzalu', 'lavazzalv', 'lavazzama', 'lavazzanl', 'lavazzapl', 'lavazzaro', 'lavazzaru', 'lavazzasa', 'lavazzase', 'lavazzasg', 'lavazzauae', 'lavazzauk', 'lavazzaus', 'lavazzaza', 'lionau-prod', 'lrsuk', 'marsbr', 'marsfr-prod', 'marsin-prod', 'marsru-prod', 'mayoraid', 'molsoncoolerca', 'molsoncoolerhr', 'molsoncoolermx', 'molsoncoorsuk', 'mondelezfr', 'ncn', 'nestlefr', 'nestleuk', 'niveauk', 'nrf2017-demo', 'one-americas-demo', 'one-apac-demo', 'pepsicoau', 'pepsicoolerus-prod', 'pmiua', 'pngau', 'pngcn-prod', 'pngjp', 'pngmccn', 'pngoccn-prod', 'pngza', 'rbde', 'rbus', 'rcuk', 'reachuk', 'repngcn-demo', 'ri-diageoau', 'rialcbevuk-prod', 'ripetcareuk-prod', 'risparkwinede', 'rius', 'rnbde', 'rnbse', 'shelluk', 'smartcooler2-trn', 'solarbr', 'static-camera-demo', 'swirecn', 'tnuvail', 'traxdemo-ts-prod')) 
	      -- and project_name in ('hbcde'))
          as SCENE_STARTED,
	#-----------------------------------------------------------------------
	(SELECT project_name,
            scene_uid,
			session_uid,
            str_to_date(concat(substr((json_extract(json_event, '$.timestamp')), 2, 19),
			     			   substr((json_extract(json_event, '$.timestamp')), 21,6)), '%Y-%m-%dT%h:%i:%s') as SCENE_CREATED_TIME
                  
	FROM eventsdb.events
                  
	WHERE date(timestamp) = date(sysdate())
	      and event_name = 'SCENE_CREATED'
	      and project_name in ('abica', 'abinbevuk', 'abitamsus-prod', 'ahold', 'americas-demo', 'batru', 'bikr', 'bimy', 'biph', 'biseask', 'bith', 'bitr', 'bivn', 'borjomiru', 'campina-prod', 'carrefourar', 'ccaau', 'ccabiza', 'ccanz', 'ccbr-prod', 'ccede', 'ccil', 'ccjp', 'cckr', 'cclc', 'ccmy', 'ccph', 'ccrpr-prod', 'ccru', 'ccrus', 'ccth', 'cctradmx', 'ccus', 'ccus-demo', 'ccvmjp', 'ccza', 'diageoau', 'diageobr', 'diageouk', 'emea-demo', 'env34', 'ferrerokr', 'hbcde', 'heinekencn', 'heinekengr', 'heinekenus-prod', 'heinzau', 'inbevar', 'inbevbe', 'InbevBr', 'InbevBr_Ontrade', 'InBevBR_Posters', 'inbevcl', 'inbevcn', 'inbevcoolerar', 'inbevcoolerus', 'inbevkr', 'inbevmx', 'inbevpy', 'inbevus', 'kcmy', 'kcus', 'labattplnoptca', 'lavazzaat', 'lavazzaau', 'lavazzabe', 'lavazzabg', 'lavazzaca', 'lavazzach', 'lavazzacl', 'lavazzacn', 'lavazzacrfr', 'lavazzacz', 'lavazzade', 'lavazzadk', 'lavazzaee', 'lavazzaes', 'lavazzafr', 'lavazzagr', 'lavazzahk', 'lavazzahr', 'lavazzail', 'lavazzait', 'lavazzajp', 'lavazzakr', 'lavazzalt', 'lavazzalu', 'lavazzalv', 'lavazzama', 'lavazzanl', 'lavazzapl', 'lavazzaro', 'lavazzaru', 'lavazzasa', 'lavazzase', 'lavazzasg', 'lavazzauae', 'lavazzauk', 'lavazzaus', 'lavazzaza', 'lionau-prod', 'lrsuk', 'marsbr', 'marsfr-prod', 'marsin-prod', 'marsru-prod', 'mayoraid', 'molsoncoolerca', 'molsoncoolerhr', 'molsoncoolermx', 'molsoncoorsuk', 'mondelezfr', 'ncn', 'nestlefr', 'nestleuk', 'niveauk', 'nrf2017-demo', 'one-americas-demo', 'one-apac-demo', 'pepsicoau', 'pepsicoolerus-prod', 'pmiua', 'pngau', 'pngcn-prod', 'pngjp', 'pngmccn', 'pngoccn-prod', 'pngza', 'rbde', 'rbus', 'rcuk', 'reachuk', 'repngcn-demo', 'ri-diageoau', 'rialcbevuk-prod', 'ripetcareuk-prod', 'risparkwinede', 'rius', 'rnbde', 'rnbse', 'shelluk', 'smartcooler2-trn', 'solarbr', 'static-camera-demo', 'swirecn', 'tnuvail', 'traxdemo-ts-prod')) 
	     --  and project_name in ('hbcde'))
          as SCENE_CREATED                              
	#-----------------------------------------------------------------------

where SCENE_STARTED.session_uid = SCENE_CREATED.session_uid
	  and SCENE_STARTED.scene_uid = SCENE_CREATED.scene_uid
      and SCENE_STARTED.SCENE_CAPTURED_TIME is not NULL
	  and SCENE_CREATED.SCENE_CREATED_TIME is not NULL
      
order by TOTAL_UPLOAD_TIME_IN_SECONDS asc;
#-----------------------------------------------------------------------
