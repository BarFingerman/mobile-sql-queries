#-----------------------------------------------------------------------
select Probe_Captured.project_name as PROJECT, 
       Probe_Captured.PROBE_IMAGE_CAPTURED_TIME, 
       Probe_Uploaded.PROBE_UPLOADED_TIME, 
       ABS(TIME_TO_SEC(TIMEDIFF(Probe_Captured.PROBE_IMAGE_CAPTURED_TIME, 
                                Probe_Uploaded.PROBE_UPLOADED_TIME))) as TOTAL_UPLOAD_TIME_IN_SECONDS
 
from (
 
    #-----------------------------------------------------------------------
	SELECT project_name,
		   image_uid,
           session_uid, 
           str_to_date(concat(substr((json_extract(json_event, '$.timestamp')), 2, 19),
			     			  substr((json_extract(json_event, '$.timestamp')), 21,6)), '%Y-%m-%dT%h:%i:%s') as PROBE_IMAGE_CAPTURED_TIME

	FROM eventsdb.events
		
	WHERE date(timestamp) < date(sysdate())
	      and event_name = 'PROBE-IMAGE_CAPTURED'
	      -- and project_name in ('abica', 'abinbevuk', 'abitamsus-prod', 'ahold', 'americas-demo', 'batru', 'bikr', 'bimy', 'biph', 'biseask', 'bith', 'bitr', 'bivn', 'borjomiru', 'campina-prod', 'carrefourar', 'ccaau', 'ccabiza', 'ccanz', 'ccbr-prod', 'ccede', 'ccil', 'ccjp', 'cckr', 'cclc', 'ccmy', 'ccph', 'ccrpr-prod', 'ccru', 'ccrus', 'ccth', 'cctradmx', 'ccus', 'ccus-demo', 'ccvmjp', 'ccza', 'diageoau', 'diageobr', 'diageouk', 'emea-demo', 'env34', 'ferrerokr', 'hbcde', 'heinekencn', 'heinekengr', 'heinekenus-prod', 'heinzau', 'inbevar', 'inbevbe', 'InbevBr', 'InbevBr_Ontrade', 'InBevBR_Posters', 'inbevcl', 'inbevcn', 'inbevcoolerar', 'inbevcoolerus', 'inbevkr', 'inbevmx', 'inbevpy', 'inbevus', 'kcmy', 'kcus', 'labattplnoptca', 'lavazzaat', 'lavazzaau', 'lavazzabe', 'lavazzabg', 'lavazzaca', 'lavazzach', 'lavazzacl', 'lavazzacn', 'lavazzacrfr', 'lavazzacz', 'lavazzade', 'lavazzadk', 'lavazzaee', 'lavazzaes', 'lavazzafr', 'lavazzagr', 'lavazzahk', 'lavazzahr', 'lavazzail', 'lavazzait', 'lavazzajp', 'lavazzakr', 'lavazzalt', 'lavazzalu', 'lavazzalv', 'lavazzama', 'lavazzanl', 'lavazzapl', 'lavazzaro', 'lavazzaru', 'lavazzasa', 'lavazzase', 'lavazzasg', 'lavazzauae', 'lavazzauk', 'lavazzaus', 'lavazzaza', 'lionau-prod', 'lrsuk', 'marsbr', 'marsfr-prod', 'marsin-prod', 'marsru-prod', 'mayoraid', 'molsoncoolerca', 'molsoncoolerhr', 'molsoncoolermx', 'molsoncoorsuk', 'mondelezfr', 'ncn', 'nestlefr', 'nestleuk', 'niveauk', 'nrf2017-demo', 'one-americas-demo', 'one-apac-demo', 'pepsicoau', 'pepsicoolerus-prod', 'pmiua', 'pngau', 'pngcn-prod', 'pngjp', 'pngmccn', 'pngoccn-prod', 'pngza', 'rbde', 'rbus', 'rcuk', 'reachuk', 'repngcn-demo', 'ri-diageoau', 'rialcbevuk-prod', 'ripetcareuk-prod', 'risparkwinede', 'rius', 'rnbde', 'rnbse', 'shelluk', 'smartcooler2-trn', 'solarbr', 'static-camera-demo', 'swirecn', 'tnuvail', 'traxdemo-ts-prod')) 
	      and project_name in ('hbcde'))
          as Probe_Captured,
	#-----------------------------------------------------------------------
	(SELECT project_name,
	        image_uid,
            session_uid,
            str_to_date(concat(substr((json_extract(json_event, '$.timestamp')), 2, 19),
			     			   substr((json_extract(json_event, '$.timestamp')), 21,6)), '%Y-%m-%dT%h:%i:%s') as PROBE_UPLOADED_TIME
                  
	FROM eventsdb.events
                  
	WHERE date(timestamp) < date(sysdate())
	      and event_name = 'PROBE_UPLOADED'
	      -- and project_name in ('abica', 'abinbevuk', 'abitamsus-prod', 'ahold', 'americas-demo', 'batru', 'bikr', 'bimy', 'biph', 'biseask', 'bith', 'bitr', 'bivn', 'borjomiru', 'campina-prod', 'carrefourar', 'ccaau', 'ccabiza', 'ccanz', 'ccbr-prod', 'ccede', 'ccil', 'ccjp', 'cckr', 'cclc', 'ccmy', 'ccph', 'ccrpr-prod', 'ccru', 'ccrus', 'ccth', 'cctradmx', 'ccus', 'ccus-demo', 'ccvmjp', 'ccza', 'diageoau', 'diageobr', 'diageouk', 'emea-demo', 'env34', 'ferrerokr', 'hbcde', 'heinekencn', 'heinekengr', 'heinekenus-prod', 'heinzau', 'inbevar', 'inbevbe', 'InbevBr', 'InbevBr_Ontrade', 'InBevBR_Posters', 'inbevcl', 'inbevcn', 'inbevcoolerar', 'inbevcoolerus', 'inbevkr', 'inbevmx', 'inbevpy', 'inbevus', 'kcmy', 'kcus', 'labattplnoptca', 'lavazzaat', 'lavazzaau', 'lavazzabe', 'lavazzabg', 'lavazzaca', 'lavazzach', 'lavazzacl', 'lavazzacn', 'lavazzacrfr', 'lavazzacz', 'lavazzade', 'lavazzadk', 'lavazzaee', 'lavazzaes', 'lavazzafr', 'lavazzagr', 'lavazzahk', 'lavazzahr', 'lavazzail', 'lavazzait', 'lavazzajp', 'lavazzakr', 'lavazzalt', 'lavazzalu', 'lavazzalv', 'lavazzama', 'lavazzanl', 'lavazzapl', 'lavazzaro', 'lavazzaru', 'lavazzasa', 'lavazzase', 'lavazzasg', 'lavazzauae', 'lavazzauk', 'lavazzaus', 'lavazzaza', 'lionau-prod', 'lrsuk', 'marsbr', 'marsfr-prod', 'marsin-prod', 'marsru-prod', 'mayoraid', 'molsoncoolerca', 'molsoncoolerhr', 'molsoncoolermx', 'molsoncoorsuk', 'mondelezfr', 'ncn', 'nestlefr', 'nestleuk', 'niveauk', 'nrf2017-demo', 'one-americas-demo', 'one-apac-demo', 'pepsicoau', 'pepsicoolerus-prod', 'pmiua', 'pngau', 'pngcn-prod', 'pngjp', 'pngmccn', 'pngoccn-prod', 'pngza', 'rbde', 'rbus', 'rcuk', 'reachuk', 'repngcn-demo', 'ri-diageoau', 'rialcbevuk-prod', 'ripetcareuk-prod', 'risparkwinede', 'rius', 'rnbde', 'rnbse', 'shelluk', 'smartcooler2-trn', 'solarbr', 'static-camera-demo', 'swirecn', 'tnuvail', 'traxdemo-ts-prod')) 
	      and project_name in ('hbcde'))
          as Probe_Uploaded                              
	#-----------------------------------------------------------------------

where Probe_Captured.image_uid=Probe_Uploaded.image_uid 
	  and Probe_Captured.session_uid=Probe_Uploaded.session_uid
      and Probe_Captured.PROBE_IMAGE_CAPTURED_TIME is not NULL
	  and Probe_Uploaded.PROBE_UPLOADED_TIME is not NULL
      
order by TOTAL_UPLOAD_TIME_IN_SECONDS asc;
#-----------------------------------------------------------------------
