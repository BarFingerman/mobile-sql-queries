Select count(event_name) as stuck_scenes
    
From eventsdb.events
	
Where event_name = 'SCENE_AGENT-UPLOAD-REJECTED' AND 
	  project_name = 'ccbr-sand' AND 
      unix_timestamp(timestamp) between unix_timestamp('2017/05/20') and 
									    unix_timestamp('2017/05/23 23:59:59') 